## BookCell
Sample app that lists books with few basic filters.

## Running the App
- Install `$ npm install -g yarn`
- Install Dependencies `$ yarn`
- Start `$ npm start`

P.S.: This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

Note:
The app tries to progressively load data to ensure initial rendering is fast enough.

## TODO
- Sorting by table columns
- Improvise performance by using [react-virtualized](https://github.com/bvaughn/react-virtualized)
- Lot of optimizations w.r.t. selector setting the book list
- Lot of tests!
