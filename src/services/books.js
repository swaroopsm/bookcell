import times from 'lodash/times';
import upperFirst from 'lodash/upperFirst';
import sample from 'lodash/sample';
import faker from 'faker';

const genderList = ['male', 'female'];

const genreList = [
  "Science fiction",
  "Satire",
  "Drama",
  "Action and Adventure",
  "Romance",
  "Mystery",
  "Horror",
  "Self help",
  "Health",
  "Guide",
  "Travel",
  "Children's",
  "Religion, Spirituality & New Age",
  "Science",
  "History",
  "Math",
  "Anthology",
  "Poetry",
  "Encyclopedias",
  "Dictionaries",
  "Comics",
  "Art",
  "Cookbooks",
  "Diaries",
  "Journals",
  "Prayer books",
  "Series",
  "Trilogy",
  "Biographies",
  "Autobiographies",
  "Fantasy",
];

// Generate a list of 100 authors so we can group / see similar authors
const authors = times(100, () => ({
  id: faker.random.uuid(),
  name: faker.name.findName(),
  gender: sample(genderList),
}));

// This can be replaced with API in the future
export function fetchBooks(count) {
  return new Promise((resolve, reject) => {
    const books = times(count, (i) => ({
      id: faker.random.uuid(),
      name: upperFirst(faker.random.words()),
      author: sample(authors),
      genre: sample(genreList),
      publishedOn: faker.date.past(),
    }));

    resolve(books);
  });
}
