export const $brandPrimary = '#ec5d57';
export const $fontPrimary = 'Lobster';
export const $fontBody = 'Open Sans, sans-serif';
export const $fontSizeBase = '1em';
export const $fontSizeLg = '2em';
export const $tableBorderColor = '#F5F5F5';
