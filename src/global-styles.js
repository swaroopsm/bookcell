import { injectGlobal } from 'styled-components';
import { $fontBody } from 'styles/variables';

/* eslint-disable no-unused-expressions */
injectGlobal`
  * {
    margin: 0;
    padding: 0;
  }

  body {
    margin: 0;
    padding: 0;
    font-family: ${$fontBody};
  }
`;
