import React from 'react';
import BookRow from 'components/BookRow';
import styled from 'styled-components';
import { $tableBorderColor } from 'styles/variables';

const Table = styled.table`
  width: 100%;
  text-align: left;
  border: 1px solid $tableBorderColor;
  
  th, td {
    padding: 15px;
    vertical-align: top;
    border-top: 1px solid ${$tableBorderColor};
  }

  thead th {
    vertical-align: bottom;
    border-bottom: 2px solid ${$tableBorderColor};
  }

  tbody {
    border-bottom: 2px solid ${$tableBorderColor};

    tr:hover {
      cursor: pointer;
      background-color: ${$tableBorderColor};
    }
  }
`;

function BookTable({ books }) {
  return (
    <Table>
      <thead>
        {/* TODO */}
        {/* Replace with dynamic columns */}
        <tr>
          <th>Title</th>
          <th>Author</th>
          <th>Genre</th>
          <th>Publish Date</th>
        </tr>
      </thead>
      <tbody>
      {
        books.map((book) => (
          <BookRow
            book={book}
            key={book.get('id')}
          />
        ))
      }
      </tbody>
    </Table>
  );
}

export default BookTable;
