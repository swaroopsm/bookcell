import React from 'react';
import styled from 'styled-components';

const Tag = styled.small`
  margin-left: 5px;
  margin-right: 5px;
  color: white;
  padding: 2px 5px;
  border-radius: 20px;
  font-size: 10px;
`

function BookRow({ book }) {
  return (
    <tr>
      <td>{book.get('name')} {book.get('tags').map((tag, index) => <Tag key={index} style={{background: tag.color}}>{tag.text}</Tag>)}</td>
      <td>{book.getIn(['author', 'name'])}</td>
      <td>{book.get('genre')}</td>
      <td>{book.get('publishedOn').toDateString()}</td>
    </tr>
  )
}

export default BookRow;
