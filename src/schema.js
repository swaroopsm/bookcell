import { schema } from 'normalizr';

const author = new schema.Entity('authors');

const book = new schema.Entity('books', {
  author,
});

export const bookList = [ book ];
