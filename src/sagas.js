import appSagas from 'containers/App/sagas';

const sagas = [].concat(
  appSagas,
);

export default sagas;
