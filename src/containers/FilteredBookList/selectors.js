import { createSelector } from 'reselect';
import { selectBooksState, selectEntitiesState } from 'containers/App/selectors';

const isHalloweenDay = (date) =>  date.getMonth() === 9 && date.getDate() === 31;

const getTags = (book) => {
  let tags = [];

  // Hardcoding the genre for now, ideally this should be picked from the list
  tags = isHalloweenDay(book.get('publishedOn')) ? tags.concat({ color: 'orange', text: 'Halloween' }) : tags;

  return tags;
};

export const selectBooks = () => createSelector(
  selectBooksState(),
  selectEntitiesState(),
  (bookIds, entities) => bookIds.map((id) => {
    const book = entities.getIn(['books', id]);

    return book.set('author', entities.getIn(['authors', book.get('author')]))
               .set('tags', getTags(book));
  })
);
