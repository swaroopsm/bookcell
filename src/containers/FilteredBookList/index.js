import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchBooks, loadHugeList } from 'containers/App/actions';
import { createStructuredSelector } from 'reselect';
import { selectBooks } from './selectors';
import BookTable from 'components/BookTable';
import styled from 'styled-components';
import { $brandPrimary, $fontBody } from 'styles/variables';

const Wrapper = styled.div`
  height: 700px;
  overflow: auto;
  margin-bottom: 15px;
`;

const Button = styled.button`
  padding: 15px;
  background: ${$brandPrimary};
  outline: 0;
  border: 0;
  color: white;
  border-radius: 2px;
  font-family: ${$fontBody};
  margin-top: 10px;
  margin-bottom: 10px;
`;

class BookList extends React.Component {
  componentDidMount() {
    this.props.fetchBooks();
  }

  render() {
    const count = this.props.books.count();

    return (
      <Wrapper>
        { count > 0 ? `Count: ${count}` : 'Loading...' }
        <div>
          <Button onClick={this.props.loadLarge}>Load 1 Million</Button>
        </div>
        <BookTable
          books={this.props.books}
        />
      </Wrapper>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  books: selectBooks(),
});

const mapDispatchToProps = (dispatch) => bindActionCreators({
  fetchBooks,
  loadLarge: loadHugeList,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(BookList);
