export const FETCH_BOOKS = 'app/FETCH_BOOKS';
export const FETCH_BOOKS_SUCCESS = 'app/FETCH_BOOKS_SUCCESS';

export const FETCH_BOOKS_NEXT_SUCCESS = 'app/FETCH_BOOKS_NEXT_SUCCESS';
export const LOAD_LARGE_SET = 'app/LOAD_LARGE_SET';
