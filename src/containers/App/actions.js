import {
  FETCH_BOOKS,
  FETCH_BOOKS_SUCCESS,
  FETCH_BOOKS_NEXT_SUCCESS,
  LOAD_LARGE_SET,
} from './constants';

export function fetchBooks() {
  return { type: FETCH_BOOKS };
}

export function finishFetchingBooks(payload) {
  return { type: FETCH_BOOKS_SUCCESS, payload };
}

export function finishFetchingNextBooks(payload) {
  return { type: FETCH_BOOKS_NEXT_SUCCESS, payload };
}

export function loadHugeList() {
  return { type: LOAD_LARGE_SET };
}
