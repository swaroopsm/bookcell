import React, { Component } from 'react';
import logo from 'logo.svg';
import FilteredBookList from 'containers/FilteredBookList';
import {
  AppContainer,
  AppLogo,
  AppHeader,
  BodyContainer,
} from './styles';

class App extends Component {
  render() {
    return (
      <AppContainer>
        <AppHeader>
          <AppLogo src={logo} />
          <h2>Welcome to BookCell</h2>
        </AppHeader>
        <BodyContainer>
          <FilteredBookList />
        </BodyContainer>
      </AppContainer>
    );
  }
}

export default App;
