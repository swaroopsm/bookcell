import { delay } from 'redux-saga';
import { takeEvery, call, put, select } from 'redux-saga/effects';
import { normalize } from 'normalizr';
import { selectAppState } from 'containers/App/selectors';
import {
  FETCH_BOOKS,
  LOAD_LARGE_SET,
} from './constants';
import {
  finishFetchingBooks,
  finishFetchingNextBooks,
} from './actions';
import { fetchBooks } from 'services/books';
import * as schema from 'schema';

function* fetchBooksWorker() {
  const appState = yield select(selectAppState());
  const total = appState.get('total');
  const perCycle = appState.get('perCycle');
  const pages = total / perCycle;

  yield delay(500);

  for (let i=1; i<=pages; i++) {
    const books = yield call(fetchBooks, perCycle);

    if (i === 1) {
      yield put(finishFetchingBooks(normalize(books, schema.bookList)));
    } else {
      yield delay(100);
      yield put(finishFetchingNextBooks(normalize(books, schema.bookList)));
    }
  }

}

function* fetchBooksWatcher() {
  yield takeEvery(FETCH_BOOKS, fetchBooksWorker);
}

function* hugeListWatcher() {
  yield takeEvery(LOAD_LARGE_SET, fetchBooksWorker);
}

export default [
  fetchBooksWatcher,
  hugeListWatcher,
];
