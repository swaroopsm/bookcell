import styled from 'styled-components';
import { $fontPrimary, $fontSizeLg } from 'styles/variables';

export const AppContainer = styled.div`
  text-align: center;
`;

export const AppLogo = styled.img`
  height: 92px;
`;

export const AppHeader = styled.header`
  height: 190px;
  padding: 20px;
  font-family: ${$fontPrimary};

  h2 {
    font-size: ${$fontSizeLg};
  }
`;

export const BodyContainer = styled.div`
  padding: 15px
`;
