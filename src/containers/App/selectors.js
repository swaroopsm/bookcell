export const selectBooksState = () => (state) => state.app.get('books');

export const selectEntitiesState = () => (state) => state.app.get('entities');

export const selectAppState = () => (state) => state.app;
