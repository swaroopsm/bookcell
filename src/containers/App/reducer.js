import {
  FETCH_BOOKS_SUCCESS,
  FETCH_BOOKS_NEXT_SUCCESS,
  LOAD_LARGE_SET,
} from './constants';
import { fromJS } from 'immutable';

const initialState = fromJS({
  entities: {},
  activeSortKey: 'todo',
  activeSortOrder: -1, // Ascending
  filters: {},
  books: [],
  perCycle: 1000,
  total: 1000 * 1,
});

function reducer(state = initialState, action) {
  switch(action.type) {
    case FETCH_BOOKS_SUCCESS: {
      const { entities, result: books } = action.payload;

      return state.withMutations(map => {
        map.set('entities', fromJS(entities))
           .set('books', fromJS(books));
      });
    }

    case FETCH_BOOKS_NEXT_SUCCESS: {
      const { entities, result: books } = action.payload;

      return state.set('entities', state.get('entities').mergeDeep(fromJS(entities)))
                  .set('books', state.get('books').concat(fromJS(books)));
    }

    case LOAD_LARGE_SET: {
      return state.set('perCycle', 10000)
                  .set('total', 10000 * 100)
                  .set('books', fromJS([]));
    }

    default: {
      return state;
    }
  }

}

export default reducer;

